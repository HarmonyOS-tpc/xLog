简单、美观、强大、可扩展的 openHormony 和 Java 日志库，可同时在多个通道打印日志，如 hilog、Console 和文件。如果你愿意，甚至可以打印到远程服务器（或其他任何地方）。

如何运行项目：
```
1.去掉config.json中的"visible": false标签
2.在library的lib中加入ohos.jar，ohos.jar可以从External Libraries中SDK文件中获取文件路径。
3.entry运行要求，通过DevEco studio,并下载openHormonySDK，将项目中的build.gradle文件中dependencies→classpath版本改为对应的版本（即你的IDE新建项目中所用的版本）
```

如何使用：
```
allprojects{
    repositories{
        mavenCentral()
    }
}
implementation 'io.openharmony.tpc.thirdlib:XLog:1.0.1'
```

XLog 能干什么:
* 全局配置（TAG，各种格式化器...）或基于单条日志的配置
* 支持打印任意对象以及可自定义的对象格式化器
* 支持打印数组
* 支持打印无限长的日志（没有 4K 字符的限制）
* XML 和 JSON 格式化输出
* 线程信息（线程名等，可自定义）
* 调用栈信息（可配置的调用栈深度，调用栈信息包括类名、方法名文件名和行号）
* 支持日志拦截器
* 保存日志文件（文件名和自动备份策略可灵活配置）
* 在 Dev Studio 中的日志样式美观
* 简单易用，扩展性高

与其他日志库的不同:
* 优美的源代码，良好的文档
* 扩展性高，可轻松扩展和强化功能
* 轻量级，零依赖


## 用法
### 初始化
#### 简单方式
```java
XLog.init(LogLevel.ALL);
```
或者如果你想要在正式版中禁止打日志
```
XLog.init(BuildConfig.DEBUG ? LogLevel.ALL : LogLevel.NONE);
```

#### 高级方式
```java
LogConfiguration config = new LogConfiguration.Builder()
    .logLevel(BuildConfig.DEBUG ? LogLevel.ALL             // 指定日志级别，低于该级别的日志将不会被打印，默认为 LogLevel.ALL
        : LogLevel.NONE)
    .tag("MY_TAG")                                         // 指定 TAG，默认为 "X-LOG"
    .enableThreadInfo()                                    // 允许打印线程信息，默认禁止
    .enableStackTrace(2)                                   // 允许打印深度为2的调用栈信息，默认禁止
    .enableBorder()                                        // 允许打印日志边框，默认禁止
    .jsonFormatter(new MyJsonFormatter())                  // 指定 JSON 格式化器，默认为 DefaultJsonFormatter
    .xmlFormatter(new MyXmlFormatter())                    // 指定 XML 格式化器，默认为 DefaultXmlFormatter
    .throwableFormatter(new MyThrowableFormatter())        // 指定可抛出异常格式化器，默认为 DefaultThrowableFormatter
    .threadFormatter(new MyThreadFormatter())              // 指定线程信息格式化器，默认为 DefaultThreadFormatter
    .stackTraceFormatter(new MyStackTraceFormatter())      // 指定调用栈信息格式化器，默认为 DefaultStackTraceFormatter
    .borderFormatter(new MyBoardFormatter())               // 指定边框格式化器，默认为 DefaultBorderFormatter
    .addObjectFormatter(AnyClass.class,                    // 为指定类添加格式化器
        new AnyClassObjectFormatter())                     // 默认使用 Object.toString()
    .addInterceptor(new BlacklistTagsFilterInterceptor(    // 添加黑名单 TAG 过滤器
        "blacklist1", "blacklist2", "blacklist3"))
    .addInterceptor(new MyInterceptor())                   // 添加一个日志拦截器
    .build();

Printer ohosPrinter = new OhosPrinter();             // 通过 openHormonyHilog 打印日志的打印器
Printer consolePrinter = new ConsolePrinter();             // 通过 System.out 打印日志到控制台的打印器
Printer filePrinter = new FilePrinter                      // 打印日志到文件的打印器
    .Builder("/sdcard/xlog/")                              // 指定保存日志文件的路径
    .fileNameGenerator(new DateFileNameGenerator())        // 指定日志文件名生成器，默认为 ChangelessFileNameGenerator("log")
    .backupStrategy(new NeverBackupStrategy()              // 指定日志文件备份策略，默认为 FileSizeBackupStrategy(1024 * 1024)
    .cleanStrategy(new FileLastModifiedCleanStrategy(MAX_TIME))     // 指定日志文件清除策略，默认为 NeverCleanStrategy()
    .flattener(new MyFlattener())                          // 指定日志平铺器，默认为 DefaultFlattener
    .build();

XLog.init(                                                 // 初始化 XLog
    config,                                                // 指定日志配置，如果不指定，会默认使用 new LogConfiguration.Builder().build()
    ohosPrinter,                                        // 添加任意多的打印器。如果没有添加任何打印器，会默认使用 OhosPrinter(Android)/ConsolePrinter(java)
    consolePrinter,
    filePrinter);
```

### 全局用法
```java
XLog.d("Simple message")
XLog.d("My name is %s", "Elvis");
XLog.d("An exception caught", exception);
XLog.d(object);
XLog.d(array);
XLog.json(unformattedJsonString);
XLog.xml(unformattedXmlString);
... // 其他全局使用
```
### 局部用法
创建一个 [Logger]。
```java
Logger partial = XLog.tag("PARTIAL-LOG")
    ... // 其他配置
    .build();
```
然后对该 [Logger] 进行局部范围的使用，所有打印日志的相关方法都跟 [XLog] 类里的一模一样。
```java
partial.d("Simple message 1");
partial.d("Simple message 2");
... // 其他局部使用
```

### 基于单条日志的用法
进行基于单条日志的配置，然后就可以直接打印日志了，所有打印日志的相关方法都跟 [XLog] 类里的一模一样。
```java
XLog.enableThreadInfo()    // 允许打印线程信息
    .enableStackTrace(3)   // 允许打印深度为3的调用栈信息
    .enableBorder()        // 允许打印日志边框
    ...                    // 其他配置
    .d("Simple message 1");

XLog.tag("TEMP-TAG")
    .enableStackTrace(0)   // 允许打印不限深度的调用栈信息
    ...                    // 其他配置
    .d("Simple message 2");

XLog.disableThreadInfo()   // 禁止打印线程信息
    .disableStackTrace()   // 禁止打印调用栈信息
    .d("Simple message 3");

XLog.enableBorder().d("Simple message 4");
```

```
Copyright 2020 Elvis Hew

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
