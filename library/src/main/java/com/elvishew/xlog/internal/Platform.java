/*
 * Copyright 2016 Elvis Hew
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.elvishew.xlog.internal;


import com.elvishew.xlog.printer.OhosPrinter;
import com.elvishew.xlog.printer.ConsolePrinter;
import com.elvishew.xlog.printer.Printer;
import ohos.hiviewdfx.HiLog;

import java.util.logging.Level;
import java.util.logging.Logger;

import static com.elvishew.xlog.Constants.LABEL_LOG;

public class Platform {
    private static final Platform PLATFORM = findPlatform();

    public static Platform get() {
        return PLATFORM;
    }

    String lineSeparator() {
        return System.lineSeparator();
    }

    Printer defaultPrinter() {
        return new ConsolePrinter();
    }

    public void warn(String msg) {
        Logger.getLogger("xLog").log(Level.WARNING, msg);
    }

    private static Platform findPlatform() {
        return new Ohos();
    }

    static class Ohos extends Platform {
        @Override
        String lineSeparator() {
            return System.lineSeparator();
        }

        @Override
        Printer defaultPrinter() {
            return new OhosPrinter();
        }

        @Override
        public void warn(String msg) {
            HiLog.warn(LABEL_LOG, msg);
        }
    }
}
