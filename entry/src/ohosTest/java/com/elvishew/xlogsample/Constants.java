package com.elvishew.xlogsample;

import ohos.hiviewdfx.HiLogLabel;

public class Constants {
    public static final HiLogLabel LABEL_LOG = new HiLogLabel(3, 0xD000F00, "[xLog] ");
}
